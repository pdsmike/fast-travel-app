const express = require('express');
const mapsClient = require('@googlemaps/google-maps-services-js').Client;
const path = require('path');
const app = express();
const maps = new mapsClient({});
const googleApiKey = 'AIzaSyBRSkujEACtfpi3ub565PytATn0txEmTA0';

app.use(express.static(path.join(__dirname, 'public')));

app.get('/places', (request, response) => {
	let query = request.query;
	if (!query.hasOwnProperty('places') || !query.hasOwnProperty('origin') || !query.places.length || !query.origin.length || !typeof query.places === 'string' || !typeof query.origin === 'string') {
		console.log('Bad request!');
		response.status(400).send('Bad request!');
		return;
	}
	let places = query.places.split(',');
	let origin = query.origin;
	let pois = [];
	/**
	 * Makes requests to Google Maps API to find POIs closest to origin
	 * @return {Promise} Promise object represents sum of requests
	 */
	function getPlaces() {
		let googleRequests = [];
		for (let i in places) {
			let options = {
				params: {
					location: origin,
					type: places[i],
					rankby: 'distance',
					key: googleApiKey
				}
			}
			let googleRequest = maps.placesNearby(options).then(data => {
				if (data.data.status === 'OK') {
					if (data.data.results.length && data.data.results[0].geometry.location) {
						pois.push(data.data.results[0].geometry.location.lat + ',' + data.data.results[0].geometry.location.lng);
						console.log(`found ${places[i]} with name "${data.data.results[0].name}"`);
						return;
					}
				}
				console.log('Could not find place or other Google error...' + data.data);
				throw data.data;
			}).catch((err) => {
				throw err;
			});
			googleRequests.push(googleRequest);
		}
		return Promise.all(googleRequests);
	}
	getPlaces().then(() => {
		/**
		 * if no rejections
		 * sending link to directions map, because google maps directions service is not free!
		 */
		//console.log('pois: ', pois);
		response.send('https://www.google.pl/maps/dir/' + origin + '/' + pois.join('/') + '/' + origin);
	}).catch(err => {
		console.dir(err);
		response.status(500).send(err.status);
	});
});

app.get('*', (request, response) => {
	response.redirect('/');
})

module.exports = app;