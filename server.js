const app = require('./app');

app.set('port', process.env.PORT || 8010);

app.listen(app.get('port'), () => {
	console.log(`listening on port ${app.get('port')}.`);
});