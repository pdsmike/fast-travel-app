/**
 * Fast Travel app
 * @author Michał Dąbrowski
 */
class FastTravel {
	constructor() {
		this.location = { lat: 52.199, lng: 20.966 };
		this.pois = [];
		this.picked = [];
		this.poiList = new MyList();
		this.pickedList = new MyList();
		this.marker = null;
		this.markers = [];
		this.welcomeScreen();
	}
	/**
	 * Sets starting params for the app
	 */
	init() {
		this.pois = ['drugstore', 'gas_station', 'convenience_store', 'restaurant', 'gym', 'bakery'];
		this.picked = [];
		this.poiList.setItems(this.pois);
		this.pickedList.setItems(this.picked);
		this.poiList.$onRemoveItem = item => {
			this.pois = this.poiList.collection;
			this.pickedList.insertItems([item]);
			this.picked = this.pickedList.collection;
		}
		this.pickedList.$onRemoveItem = item => {
			this.picked = this.pickedList.collection;
			this.poiList.insertItems([item]);
			this.pois = this.poiList.collection;
		}
	}
	/**
	 * Creates dialog window to show quick messages
	 * @param  {String} title   message title
	 * @param  {String} message massage body
	 */
	infoScreen(title, message) {
		let dialog = new DialogWindow();
		let textTitle = document.createElement('h3');
		textTitle.innerHTML = title;
		let text = document.createElement('p');
		text.innerHTML = message;
		let button = document.createElement('button');
		button.innerHTML = 'OK';
		button.classList.add('btn', 'btn-alert');
		button.onclick = dialog.close.bind(dialog);
		dialog.add(textTitle);
		dialog.add(text);
		dialog.add(button);
	}
	/**
	 * Creates a welcome screen
	 */
	welcomeScreen() {
		this.infoScreen('Welcome to Fast Travel App!', 'Please click on map to select your location, or turn on GPS service and click "Get GPS location" button.');
	}
	/**
	 * Initiates Google Map
	 * @param  {String} elementId id of DOM element, where map is attached
	 */
	initMap(elementId) {
		let element = document.getElementById(elementId);
		if (!element) {
			console.error('Can not find map element!');
		}
		this.map = new google.maps.Map(element, { center: this.location, zoom: 12 });
		this.map.addListener('click', this.setLocation.bind(this));
	}
	/**
	 * Creates dialog interface to pick POIs
	 */
	travelPlanner() {
		this.init();
		if(!this.marker) {
			this.infoScreen('Warning', 'You need to select your location first.');
			return;
		}
		let dialog = new DialogWindow();
		let text = document.createElement('h3');
		let button = document.createElement('button');
		text.innerHTML = 'Available places: ';
		dialog.add(text);
		dialog.add(this.poiList.parent);
		text = document.createElement('h3');
		text.innerHTML = 'Selected places: ';
		dialog.add(text);
		dialog.add(this.pickedList.parent);
		dialog.add(document.createElement('h3'));
		button.innerHTML = 'Show directions';
		button.classList.add('btn', 'btn-success');
		button.onclick = () => {
			if(this.picked.length===0) return;
			this.findRoute();
			dialog.close();
		}
		dialog.add(button);
	}
	/**
	 * Handles map click
	 * @param {Object} event Google Maps event object
	 */
	setLocation(event) {
		this.placeMarker(event.latLng);
	}
	/**
	 * Uses client's geolocation service
	 */
	getGPSLocation() {
		if (window.navigator.geolocation) {
			const geo = window.navigator.geolocation;
			geo.getCurrentPosition(position => {
				this.placeMarker(new google.maps.LatLng({ lat: position.coords.latitude, lng: position.coords.longitude }));
			}, () => {
				this.infoScreen('Warning', 'GPS service not availabe.');
			});
		} else {
			this.infoScreen('Warning', 'Your browser does not support location service.');
		}
	}
	/**
	 * Places marker on map and sets starting location
	 * @param  {[Object]} position Google Maps LatLng object
	 */
	placeMarker(position) {
		if(!this.marker) {
			this.marker = new google.maps.Marker();
		}
		this.location = position;
		this.marker.setPosition(this.location);
		this.marker.setMap(this.map);
		this.map.panTo(this.location);
	}
	/**
	 * Handles search request
	 */
	findRoute() {
		let request = this.getData();
		request.onreadystatechange = () => {
			if (request.readyState === 4) {
				if(request.status === 200) {
					let dialog = new DialogWindow();
					let url = document.createElement('a');
					url.href = request.responseText;
					url.target = '_blank';
					url.innerHTML = 'Click to navigate';
					dialog.add(url);
					return;
				}
				let responseText = request.responseText
				if(request.status === 0) responseText='No response from server!';
				this.infoScreen('Error '+request.status, responseText);
			}
		}
		request.timeout = () => {
			this.infoScren('Error', 'There is a problem with network connection... Timed out.');
		}
	}
	/**
	 * Makes a search request to the back-end
	 * @return {Object} XHR Object
	 */
	getData() {
		let places = 'places='+this.picked.join(',');
		let origin = 'origin='+this.location.toUrlValue();
		let query = '?'+[origin, places].join('&');
		let req = new XMLHttpRequest();
		req.open('GET', '/places' + encodeURI(query));
		req.setRequestHeader('Accept', 'text/html');
		req.send();
		return req;
	}
};