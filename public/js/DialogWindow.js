/**
 * DialogWindow
 * @author Michał Dąbrowski
 */
class DialogWindow {
	constructor() {
		this.panelId = 'DialogPanel' + Date.now();
		this.panel = document.createElement('div');
		this.panel.style = 'position: fixed; left:0; top:0; width: 100%; height: 100%; background: rgba(0,0,0,0.7);';
		this.panel.id = this.panelId;
		this.window = document.createElement('div');
		this.window.classList.add('panel', 'panel-default', 'text-center');
		this.window.style = "position: absolute; min-width: 50vw; top: 50%; left: 50%; transform: translate(-50%, -50%); padding: 2em;";
		this.panel.appendChild(this.window);
		this.content = document.createElement('div');
		this.content.classList.add('d-block');
		this.window.appendChild(this.content);
		this.closeButton = document.createElement('span');
		this.closeButton.style = "position: absolute; top: 0; right: 0.5em; font-size: 100%; cursor: pointer";
		this.closeButton.innerHTML = 'X';
		this.closeButton.addEventListener('click', this.close.bind(this));
		this.window.appendChild(this.closeButton);
		document.getElementsByTagName('body')[0].appendChild(this.panel);
	}
	add(DOMelement) {
		this.content.appendChild(DOMelement);
	}
	close() {
		this.closeButton.removeEventListener('click', this.close.bind(this));
		this.panel.remove();
	}
}
