/**
 * MyList
 * @author Michał Dąbrowski
 */
class MyList {
	constructor(elementId) {
		this.collection = [];
		this.parent = document.getElementById(elementId);
		if (!this.parent) {
			this.parent = document.createElement('div');
		}
		this.init();
	}
	init() {
		// remove all buttons
		while (this.parent.childNodes.length) {
			// remove listeners to avoid memory leaks
			this.parent.childNodes[0].removeEventListener('click', this.removeItem.bind(this));
			// remove the DOM element
			this.parent.removeChild(this.parent.childNodes[0]);
		}
		// create new buttons
		for (let i in this.collection) {
			let element = document.createElement('button');
			element.classList.add('btn', 'btn-default');
			let textNode = document.createTextNode(this.collection[i].replace('_', ' '));
			element.dataset.value = this.collection[i];
			element.appendChild(textNode);
			element.addEventListener('click', this.removeItem.bind(this));
			this.parent.appendChild(element);
		}
	}
	setItems(arr) {
		this.collection = arr;
		this.init()
	}
	insertItems(arr) {
		this.collection = [...this.collection, ...arr];
		this.init();
	}
	removeItem(event) {
		this.collection = this.collection.filter(item => {
			if (item === event.target.dataset.value) {
				return false;
			}
			return true;
		});
		this.init();
		this.$onRemoveItem(event.target.dataset.value);
	}
	$onRemoveItem() {}
}
